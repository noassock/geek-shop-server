import DBConnector from "../database.js";

export const getProducts = () => {
    return DBConnector.promise().query('SELECT product_code, description, price FROM product');
};

export const postProducts = (product) => {
    return DBConnector.promise().query('INSERT INTO product(product_code, description, price) VALUES (?, ?, ?)', [product.product_code, product.description, product.price]);
}