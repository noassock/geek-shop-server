import DBConnector from "../database.js";

export const getAllVouchers = () => {
    return DBConnector.promise().query('SELECT name, reduction FROM voucher');
};

export const getVoucher = (voucher) => {
    return DBConnector.promise().query('SELECT name, reduction FROM voucher WHERE name = ?', voucher.name);
};

export const postVoucher = (voucher) => {
    return DBConnector.promise().query('INSERT INTO voucher(name, reduction) VALUES (?, ?)', [voucher.name, voucher.reduction]);
};

export const patchVoucher = (voucher) => {
    return DBConnector.promise().query('UPDATE voucher SET reduction = ? WHERE name = ?', [voucher.reduction, voucher.name]);
};

export const deleteVoucher = (voucher) => {
    return DBConnector.promise().query('DELETE FROM voucher WHERE name = ?', voucher.name);
};