export function logRouteCall(req, res, next) {
    console.log(`Someone is trying to use ${req.method} on ${req.originalUrl}`);
    next();
};

export function checkVoucherReqBody(req, res, next) {

    // Récupérer les valeurs du coupons dans le body de la req
    if (req.body && req.body.name && req.body.reduction) {

        req.voucher = {name: req.body.name, reduction: req.body.reduction};
        next();

    } else {
        res.send(`${req.method} FAILED :( `);
    }
}