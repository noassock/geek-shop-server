//Declare routes here
import express from 'express';

import {
    getProducts,
    postProducts,
} from './models/Products.js';

import {
    getAllVouchers,
    getVoucher,
    postVoucher,
    patchVoucher,
    deleteVoucher,
} from './models/Vouchers.js';

import {
    logRouteCall,
    checkVoucherReqBody,
} from './middlewares.js';

const router = express(); //création d'une instance d'express;

router.use(logRouteCall);

router.route('/')
    .get((req, res) => {
        res.send("Hello World !");
    });

//Routes for products
router.route('/products')
    // .all(middleware) // to apply a middleware on a route for all methods
    .get((req, res) => {
            getProducts()
                .then((product) => {
                    res.json(product[0]);
                })
                .catch((error) => {
                    res.send(error.message);
                })
        }).post((req, res, next) => {

        if (req.body && req.body.product_code && req.body.description && req.body.price) {
            postProducts(req.body);
        }
        res.send();
    })

//Routes for vouchers
router.route('/vouchers')
    .get((req, res) => {
        getAllVouchers()
            .then((voucher) => {
                res.json(voucher[0]);
            })
            .catch((error) => {
                res.send(error.message);
            })
    })
    .post(
        checkVoucherReqBody,
        (req, res, next) => {
        postVoucher(req.voucher);
        res.send(`${req} SUCCEED :) `);
    })
    .patch((req, res, next) => {
        if (req.body && req.body.name && req.body.reduction) {
            const voucher = {
                name: req.body.name,
                reduction: req.body.reduction
            };
            patchVoucher(voucher);
        }
        res.send();
    })
    .put((req, res, next) => {
        if (req.body && req.body.name && req.body.reduction) {
            const voucher = {
                name: req.body.name,
                reduction: req.body.reduction
            };
            getVoucher(voucher)
                .then((product) => {
                    if (product[0].length) {
                        patchVoucher(voucher);
                    } else {
                        postVoucher(voucher);
                    }
                    res.send();
                })
                .catch((error) => {
                    res.send(error.message);
                })
        }
    })
    .delete((req, res, next) => {
        if (req.body && req.body.name && req.body.reduction) {
            const voucher = {
                name: req.body.name,
                reduction: req.body.reduction
            };
            deleteVoucher(voucher);
        }
        res.send();
    })

export default router;