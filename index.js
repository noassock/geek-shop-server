import dotenv from 'dotenv';
import express from 'express';
import router from './router.js';
import cors from 'cors';

const app = express(); //création d'une instance d'express;
dotenv.config(); //dotenv va chercher sa configuration dans le fichier d'environnement

app.use(express.urlencoded({ //parser les urls et les body
    extended: true
}));

const port = process.env.PORT || 3001;

app.use(cors());
app.use(router);

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`);
});

